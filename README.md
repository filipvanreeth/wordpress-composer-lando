# WordPress via Composer & Lando

## What do you need? (preinstalled)

### Lando

[Lando](https://lando.dev) is for developers who want to:

- Quickly specify and painlessly spin up the services and tooling needed to develop all their projects.
- Ship these local development dependencies in a per-project, lives-in-git config file
- Automate complex build steps, testing setups, deployments or other repeated-more-than-once workflows
- Avoid the built-in-masochism of directly using `docker` or `docker-compose`

> **Lando is a development tool!**
>
>Note that while you can run Lando in production, it is highly discouraged, not recommended and 100% not supported! DON'T DO IT!

When you [install Lando](https://github.com/lando/lando/releases) it will also install Docker.

This repository has 3 different branches with a specific Lando configuration:

1. [wp-native](-/tree/install-wp-native) - This is the most barebone configuration. It will install WordPress in the root of the project and will use the default Lando configuration for PHP (7.4), MySQL (5.7) and Apache.
2. [wp-native-extended](-/tree/install-wp-native-extended) - This configuration will install WordPress in a `web` folder by using an extended Lando configuration for PHP (8.0), MySQL (8.0) and NGINX.
3. [wp-bedrock](-/tree/install-wp-bedrock) - This configuration will install [Bedrock](https://roots.io/bedrock/) in the root of the project and will use and extended Lando configuration for PHP (8.0), MySQL (8.0) and NGINX.